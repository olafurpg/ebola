\section{Humanitarianism} % (fold)
\label{sec:Background}
Humanitarianism as an ideology, movement and profession can be interpreted in many ways.
Recent studies (e.g.,~\citeauthor{barnett_humanitarianism_2005} (2005)) reveal that humanitarianism has been greatly transformed over the recent decades.
In summary, the transformation for humanitarianism has gone from impartiality to becoming a politicized and institutionalized enterprise.
% A philisophy introduced by \autocite{united_nations_charter_1945}, is to define a humanitarian as a person willing and able to help other in the face of disaster or crisis.
% From this point of view, with regards to Ebola, a simplistic interpretation of humanitarianism would read as a movement of willing and able people (think: in the eye of the beholder) united in a mission to stop the outbreak of the EVD.\
% In reality however, humanitarianism is more complicated.
% Every humanitarian is driven by her own motivations, works by her own principles and uses different standards of accountability.
In this section, we build our vocabulary for discussing these various facets of humanitarianism, which will become relevant in later sections.

% Much research has been made on humanitarianism, cite bunch of papers:

In what follows, we use the framework on meta functions of humanitarianism in a globalised world developed by~\citeauthor{donini_far_2010} (2010).
The definitions below are in some cases summarized and other cases expanded to suit the discussion of the Ebola epidemic.
To begin with, we introduce two broad categories of humanitarianism borrowed from~\citeauthor{donini_far_2010}~(2010, p.\ 220): the \emph{established institutions} and the \emph{``other humanitarianisms''}.

\subsection{The established humanitarianisms} % (fold)
\label{sub:established}
The established humanitarianisms are a collection of a wide range of organizations that aim to aid people in the face of disaster or crisis.
Being established, these organizations have the resources and ambitions to make their work visible to both those who are on the receiving end of their work as well as the ones sponsoring their work.
The characteristics of established institutions borrow many similarities from capitalistic institutions, in the sense that they compete and co-operate in a marketplace of humanitarian organizations in order to accomplish their private institutional goals.

As an ideology, movement and profession, the whole spectrum of humanitarianism can be found within established institutions.
Inexperienced members may be attracted by the ideology of the organization they decide to join.
For the organization to remain relevant --- and competitive in the market of humanitarian organizations --- the organization must professionalize its members to become experts in their field.
Similarly, for successful movements to remain successful, they develop into structured organizations with clearly defined standards and goals.
Established institutions however, although they may appear similar in shape, are motivated by different sets of ideologies.
We cover four of such ideologies in this essay: \emph{Dunantists}, \emph{Wilsonians}, \emph{Solidarists} and \emph{faith-based}.

\subsubsection{Dunantists} % (fold)
\label{sub:dunantists}
The Dunantists ideology has its origins from the founder of the Red Cross Henri Dunant~\autocite[p.\ 728]{barnett_humanitarianism_2005}.
Dunantist organizations define humanitarianism, as phrased by Barnett, as \emph{``the neutral, independent, and impartial provision of relief to victims of conflict and believe that humanitarianism and politics must be segregated''}~\autocite[p.\ 728]{barnett_humanitarianism_2005}.
According to Donini, an important distinction of Dunantist organizations in the context of the Ebola epidemic, is that they believe in the separation of humanitarianism and politics.
Another critical aspect of Dunantists organizations is their views on neutrality, the philosophy that their relief efforts should not be influenced by the agenda of anyone except the people in need of aid.
Neutrality is expressed as a key component in both the charter of MSF~\autocite{medecins_sans_frontieres_msf_international_msf_????} and the Red Cross~\autocite{international_red_cross_and_red_crescent_movement_statutes_2005}, two well known Dunantists organizations which have played key roles in the Ebola epidemic.

\subsubsection{Wilsonians} % (fold)
\label{sub:Wilsonians}
The Wilsonian ideology origins from the mandate of former US president Woodrow Wilson, who sponsored loans for billions of dollars to aid the development of many European allies countries during and after the First World War~\autocite[p.\ 728]{barnett_humanitarianism_2005}.
A key characteristic of Wilsonian organizations is their close co-operation with governments, and how the goals of Wilsonian organizations often closely align with a government's foreign-policy.
This alignment with government-policy clearly separates Wilsonians from Dunantists.
Wilsonian organizations often act in the name of peace and progress, as peace and progress happen to be defined by the organization itself --- or the government sponsoring the actions.
Wilsonian organizations can be considered pragmatic in many ways however, and they resemble Dunantists organizations in this regard, since they commonly offer expertise in logistics and perform tasks that are technical in nature.

\subsubsection{Solidarists} % (fold)
\label{sub:Solidarists}
Solidarists are more political in nature than Dunantists and Wilsonians and more upfront with their intentions.
In the words of Donini, solidarists \emph{``pursue a range of advocacy, development, human rights and/or justice objectives in addition to humanitarian assistance''}~\autocite[][p.\ s222]{donini_far_2010}.
The difference between solidarists and Wilsonians perhaps, can be explained in the way that although they take a political stance, they are more independent from their respective governments compared to their Wilsonian partners.
Solidarists are often motivated by a change.

\subsubsection{Faith based} % (fold)
\label{sub:Faith_based}
Faith based humanitarianism is a diverse range of humanitarianisms that share the common trait of being motivated by religious reasons.
Although often categorized as a separate humanitarianism, as I do in this essay, the concept of faith based humanitarianism is somewhat orthogonal to the three ideologies discussed above.
The concepts are orthogonal since a religious humanitarian organization could follow any of the three ideologies presented above, or vice-versa.

\subsubsection{Alternative divisions} % (fold)
\label{sub:Limitations}
% Established humanitarian organisations have been a widely studied and 
It is important to note that these ideologies presented here are not exhaustive to explain the intricate differences between established humanitarian institutions.
For instance, as quoted in~\citeauthor{stoddard_humanitarian_2009} (2009), Weiss\footnote{Weiss, Thomas G. 1999. Principles, politics, and humanitarian action. Ethics
\& International Affairs 13:1–22.} uses a different spectrum for ideologies that ranges from ``classicists'', to ``minimalists'', ``maximalists'' and ``solidarists''.
From Weiss's point of view, the International Committee of the Red Cross and MSF --- which both fall into the Dunantist camp --- belong to the opposite side of the spectrum.
Another typology introduced by O'Malley and Dijkzeul~\autocite{omalley_typology_2002}, plots humanitarian organisations in two dimensions, from ``independent'' to ``public service contractor'' in one dimension and ``impartial'' to ``solidarity'' on the other.
Figure~\ref{fig:limitations} shows details of these two spectra.
Observe that these two spectra do not diminish the Dunantist, Wilsonian and Solidarist definitions, but rather extend them.
\begin{figure}\label{fig:limitations}
    \centering{}
    \includegraphics[width=0.45\linewidth]{img/weiss.png}
    \includegraphics[width=0.45\linewidth]{img/omalley.png}
    \caption{Weiss's spectrum on the left and O'Malley and Dijkzeul's spectrum on the right. Source~\autocite{stoddard_humanitarian_2009}.}
\end{figure}

\subsection{The other humanitarianisms} % (fold)
\label{sub:Unestablished}
By its definitions as an ideology, movement and profession, the borders of humanitarianism are not limited to ``established institutions''.
Therefore, one can imagine multiple black, or hidden, ecosystems of actors who are willing and able to provide aid to people in need~\autocite{donini_far_2010}.
Examples of such aid include:
\begin{itemize}
    \item ad-hoc local effort or community movements
    \item governmental aid to its own citizens in face of unexpected crisis
\end{itemize}
The undefined nature of the other humanitarianism makes it hard to summarize.
However, the contributions performed by the other humanitarianism cannot be understated.
Every act of a willing and able person to assist others in need, without affiliation to an established institution, falls into this category.

\subsection{Donini's meta functions} % (fold)
\label{sub:Meta_functions}
In his paper ``The far side'',~\citeauthor{donini_far_2010} (2010) discusses \emph{meta functions} of humanitarian actions.
He recognizes the limitations of his definitions, and suggests that his contribution is merely a first approximation.
In later chapters of this essay, I will put Donini's contribution to the test and see how accurately the meta functions capture humanitarianism in the Ebola epidemic.

Donini introduces three kinds of meta functions: \emph{macro}, \emph{meso} and \emph{micro}.


\subsubsection{Macro functions} % (fold)
\label{sub:macro}
Although the underlying goal of some humanitarian action may be well understood, the actions have intended or unintended side-effects, which we'll refer to as macro functions.
By definition, macro functions go beyond the efforts of individuals and consider the effects of collective humanitarian actions.
Given the nature of macro functions, aid workers who perform the actions may not be aware of this macro effect.
The macro function is nevertheless performed and can have a significant impact on the local and international community.
Donini lists three main macro functions.

\begin{enumerate}[1.]

    \item \emph{Humanitarian action can be an influential tool to spread culture.}

        When humanitarian organizations approach a crisis, they also bring their approach to management, life principles and leisure.
        Historically, humanitarian organizations have had Western origins by and large.
        Humanitarianism has therefore contributed to the Western influence in globalisation in some regard.
        However, this effect is not limited to Western countries.
        For instance, consider the China-African Medical Cooperation, which aims ``to serve Africans the Chinese way''~\autocite{anshan_china-africa_????}, which is a manifestation of this first macro effect.

    \item \emph{Western humanitarianism becomes the dominant discourse.}

        By performing humanitarian action according to Western/Northern principles, those principles become the standard by which any humanitarian aid is judged.
        This observation comes from the fact that the institutional humanitarianisms determine the term of engagement.
        The more prevalent the ways of western Dunantist, Wilsonian and Solidarist humanitarians, the less room there is for alternative humanitarianisms to exist.
        When alternative ideologies struggle to be considered, alternative interpretations of humanitarianism become limited.
        An institutional, homogeneous humanitarianism, likely inherited from war times or colonial periods, is left to thrive.

    \item \emph{The established institutional humanitarianism can aid to preserve the existing distant North-South relationship, as opposed to narrowing the gap between the developed and underdeveloped.}

        This effect may seem paradoxical at first thought: since much humanitarian effort involves development support (in the spirit of Woodrow Wilson), how can that effort do the opposite of what it aims to accomplish?
        This idea comes from two observations.
        Firstly, consider the relationship of donors and humanitarian organizations.
        Since much humanitarian aid is sponsored by wealthy donors, i.e., middle-class Westerners or capitalistic enterprises, there may be more incentive for the humanitarian organization to only appear successful while maintaining the status quo to rationalize the donations.
        Secondly, humanitarianism can serve as a tool for Northern governments to weaken the sovereignty of the country receiving the aid.
        Mark Duffield explains this effect: \emph{``as heirs of the liberal tradition, international NGOs played an important part in contesting state-led modernization''}~\autocite[p. 233. Quote borrowed from Donini on page s229]{duffield_development_2007}.
\end{enumerate}

It is worth to note that the three macro functions discussed above do not necessarily have to be negative.
The institutional humanitarianism has in many cases been successful in spreading values that strengthen a society in need, which may have previously suffered from oppression, lack of means for health care, or education.

\subsubsection{Meso functions} % (fold)
\label{sub:meso}
The second meta function introduced by Donini are so-called \emph{meso} functions.
According to the New Oxford American English Dictionary~\autocite{jewell_new_2001}, the word meso is commonly combined other words to carry the meaning of middle or intermediate.
Extrapolating from this definition, meso functions fall into the gap between macro functions and micro functions.

According to Donini, meso functions relate to the concrete impacts of humanitarian actions on North-South relationships, as opposed to the somewhat more philosophical nature of macro functions.
Donini lists three meso functions, which relate with each other in many ways.

\begin{enumerate}[1.]
    \item \emph{Humanitarian aid makes countries safe for capital.}

        Humanitarian aid that requires massive funding and is performed in the name of progress and modernization clearly has an economic impact.
        The inflow of money and involved people from such aid will open the receiving community for new business opportunities.
        Therefore, disasters and crises can paradoxically strengthen the victims' economy instead of weakening it\footnote{Note that this contradicts in some way with the third meso function. Moreover, this meso function could by itself have unintended side-effects since it may become favorable to start a crisis or disaster to invite flow of capital.}.

    \item \emph{Humanitarianism becomes part of the government.}

        Many accomplished members of humanitarian organizations have successfully transitioned their careers into politics or policy making positions.
        % The members involvement with humanitarian activities have promoted their s

    \item \emph{Humanitarianism becomes a form of power capable of manipulating policy and governance.}

        This meso function is an implication of the previous two meso functions.
        For instance, the outcome of such power may be that certain projects, which fit the agenda of a relevant humanitarian agency, get prioritized.
\end{enumerate}
In combination with the third macro function, a manifestation of meso functions can come in the form of weakened government and state privatization.

\subsubsection{Micro functions} % (fold)
\label{sub:micro}
Micro functions deal with the impacts that humanitarianism has on the lives of individuals who participate in humanitarianism as professionals or volunteers.
The motivations for being a \emph{humanitarian} --- an active member of humanitarianism --- can vary dramatically from person to person.
Humanitarianism attracts a diverse group of people, ranging from lapsed revolutionaries, religious members, military personnel, academics, researchers and common people.
For these people, humanitarianism can offer many things including
a full-time profession with opportunities for career advancements and making a living;
an exciting lifestyle and an opportunity to experience adventures; or
an opportunity to fulfill their religious ambitions and responsibilities.

