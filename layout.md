# Mapping humanitarian meta functions surrounding the Ebola virus epidemic
How successfully Donini has described the situation.

## Hostility towards intervention

## Mapping the actors
NGOs, WHO, UN, Local.

## Two broad groups of Humanitarian components

* Established
    * Dunantists
    * Wilsonian
* Unestablished
    * Ecowas?
    * Music

# Three 'Cs'
* Compassion
* Change
* Containment
    * Relevant for Ebola

## Communication network of each actor

* Heroes
    * Health workers (from abroad)
* Victims
    * Local people (family)
* Villains
    * Virus (not curse)
    * Local tradition (doesn't work)

## Hans Rosling
* Heroes
    * The Government
    * "Africans"
    * Locals who gained the trust
* Victims
    * Little emphasis
* Villains
    * Little emphasis

# Types of humanitarianism
* Professional
    * US/Chinese/UK soldies
    * WHO
    * MSF
    * Emergency
* Movement
    * Stop ebola songs
        * King Mensah
        * Y en marre
* Ideological

Hans Rosling? Researcher.
Bill Gates? 60m USD. Billionaire.
UNC’s Dr. William Fischer? Researcher.

## Hesperian
## Hans Rosling

"One good thing that came out of the Ebola epidemic, is that it served as a wake up call"

## Meta motivations
The motivations for stopping the Ebola virus spread can appear obvious.
But what are the meta-motivations.

* Macro
    * Professional
* Meso
* Micro
    * Hans Rosling

# Conflicts between local and international NGOs

"To put out this fire, we must run into the burning building"
